#!/bin/bash
set -ex
sudo rm -rf build
mkdir -p build
pushd build
cmake ..
make
