#include <iostream>
#include <string>
#include <cmath>

using namespace std;

template<typename T> unsigned int bitshift(T bit, const unsigned int crit) {
    return (unsigned int)floor((unsigned int)bit/crit);
}

template<typename T> unsigned int bitcrop(T bit, const unsigned int crit) {
    return (unsigned int)(floor((unsigned int)bit/crit)*crit);
}

template<typename T> unsigned int get_crit() {
    return (unsigned int)pow(2, (unsigned int)(sizeof(T)*4));
}

template<typename T1, typename T2> T1 bitcompose(T2 bit1, T2 bit2) {
    return (T1)((unsigned int)bit1 + (unsigned int)bit2);
}

template<typename T1, typename T2> T1 bitextract(T2 bit, const unsigned int crit) {
    return (T1)(((unsigned int)bit % crit)*crit);
}

template<typename T> T bithide(T hider, T hiding) {
    const unsigned int crit = get_crit<T>();
    const unsigned int shifted = bitshift(hiding, crit);
    const unsigned int cropped = bitcrop(hider, crit);
    T composed = bitcompose<T>(cropped, shifted);
    return composed;
}

template<typename T1, typename T2> T1 bitreveal(T2 hider) {
    const unsigned int crit = get_crit<T2>();
    T1 revealed = bitextract<T1>(hider, crit);
    return revealed;
}

template<typename T> T get_hiding() {
    return (T)(pow(2, sizeof(T)*6));
}

template<typename T> T get_hider() {
    return (T)(pow(2, sizeof(T)*8));
}

int main() {
    unsigned char hider[12];
    const int hider_size = (int)(sizeof(hider)/sizeof(hider[0]));
    unsigned char hiding[hider_size/2], hidden[hider_size], revealed[hider_size];
    const int hiding_size = (int)(sizeof(hiding)/sizeof(hiding[0]));

    if (hiding_size > hider_size) {
        cerr << "hiding size must not be larger than hider size" << endl;
        return 1;
    }

    for (int i = 0; i < hider_size; i++) {
        hider[i] = get_hider<unsigned char>();
    }

    for (int i = 0; i < hiding_size; i++) {
        hiding[i] = get_hiding<unsigned char>();
    }

    for (int i = 0; i < hider_size; i++) {
        if (i < hiding_size) {
            hidden[i] = bithide(hider[i], hiding[i]);
            cout << hider[i] << " hiding "
                 << hiding[i] << " => "
                 << hidden[i] << endl;
        } else {
            hidden[i] = (unsigned char)bitcrop(hider[i], get_crit<unsigned char>());
        }
    }

    for (int i = 0; i < hider_size; i++) {
        revealed[i] = bitreveal<unsigned char>(hidden[i]);
    }

    int not_recovered = 0;
    for (int i = 0; i < hiding_size; i++) {
        cout << hidden[i] << " revealed bits: " << revealed[i];
        if (hiding[i] != revealed[i]) {
            not_recovered = 1;
            break;
        }
        cout << " matching " << hiding[i] << endl;
    }
    return not_recovered;
}
