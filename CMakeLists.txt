cmake_minimum_required(VERSION 3.0.1)
set(CMAKE_CXX_STANDARD 14)
project(steganography_toy CXX)

add_subdirectory(src)
